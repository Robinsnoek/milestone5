#!/bin/bash
# Descr: Returns frequency of the word "de"
#
# Usage: ./sample.sh FILE
#

FILE=$1

cat $FILE | grep -iwo "de" | wc -l